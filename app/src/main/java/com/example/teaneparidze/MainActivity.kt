package com.example.teaneparidze

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    private val TAG: String = "MY_TAG"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        log.d(TAG, msg:"onCreate")
    }


    override fun onStart(){
        super.onStart()

        Log.d(TAG,msg: "onStart")
    }
    override fun onResume(){
        super.onResume()

        Log.d(TAG,msg: "onResume")
    }

    override fun onPause(){
        super.onPause()

        Log.d(TAG,msg: "onPause")

    }

    override fun onStop(){
        super.onStop()

        Log.d(TAG,msg: "onStop")

    }

    override fun onRestart(){
        super.onRestart()

        Log.d(TAG,msg: "onRestart")

    }

    override fun onDestroy(){
        super.onDestroy()

        Log.d(TAG,msg: "onDestroy")

    }



}
